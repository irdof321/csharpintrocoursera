﻿using System;
using ConsoleCards;

namespace ProgrammingAssignment3
{
    // IMPORTANT: Only add code in the section
    // indicated below. The code I've provided
    // makes your solution work with the 
    // automated grader on Coursera

    /// <summary>
    /// Programming Assignment 3
    /// </summary>
    class Program
    {
        /// <summary>
        /// Programming Assignment 3
        /// </summary>
        /// <param name="args">command-line args</param>
        static void Main(string[] args)
        {
            // loop while there's more input
            string input = Console.ReadLine();
            while (input[0] != 'q')
            {
                // Add your code between this comment
                // and the comment below. You can of
                // course add more space between the
                // comments as needed

                // declare a deck variables and create a deck object
                // DON'T SHUFFLE THE DECK
                Deck deck = new Deck();


                // deal 2 cards each to 4 players (deal properly, dealing
                // the first card to each player before dealing the
                // second card to each player)
                Card[] player1 = new Card[2];
                Card[] player2 = new Card[2];
                Card[] player3 = new Card[3];
                Card[] player4 = new Card[3];

                player1[0] = deck.TakeTopCard();
                player2[0] = deck.TakeTopCard();
                player3[0] = deck.TakeTopCard();
                player4[0] = deck.TakeTopCard();

                player1[1] = deck.TakeTopCard();
                player2[1] = deck.TakeTopCard();
                player3[1] = deck.TakeTopCard();
                player4[1] = deck.TakeTopCard();




                // deal 1 more card to players 2 and 3
                player3[2] = deck.TakeTopCard();
                player4[2] = deck.TakeTopCard();


                // flip all the cards over
                player1[0].FlipOver();
                player1[1].FlipOver();

                player2[0].FlipOver();
                player2[1].FlipOver();

                player3[0].FlipOver();
                player3[1].FlipOver();
                player3[2].FlipOver();

                player4[0].FlipOver();
                player4[1].FlipOver();
                player4[2].FlipOver();



                // print the cards for player 1
                Console.WriteLine(player1[0].Rank + "," + player1[0].Suit);
                Console.WriteLine(player1[1].Rank + "," + player1[1].Suit);

                // print the cards for player 2
                Console.WriteLine(player2[0].Rank + "," + player2[0].Suit);
                Console.WriteLine(player2[1].Rank + "," + player2[1].Suit);
                Console.WriteLine(player3[2].Rank + "," + player3[2].Suit);
                Console.WriteLine(player3[0].Rank + "," + player3[0].Suit);
                Console.WriteLine(player3[1].Rank + "," + player3[1].Suit);
                Console.WriteLine(player4[2].Rank + "," + player4[2].Suit);
                Console.WriteLine(player4[0].Rank + "," + player4[0].Suit);
                Console.WriteLine(player4[1].Rank + "," + player4[1].Suit);





                // print the cards for player 1
             /*   Console.WriteLine(player1[0].Rank + "," + player1[0].Suit);
                Console.WriteLine(player1[1].Rank + "," + player1[1].Suit);*/

                // print the cards for player 2
              /*  Console.WriteLine(player2[0].Rank + "," + player2[0].Suit);
                Console.WriteLine(player2[1].Rank + "," + player2[1].Suit);*/


                // print the cards for player 3
              /*  Console.WriteLine(player3[0].Rank + "," + player3[0].Suit);
                Console.WriteLine(player3[1].Rank + "," + player3[1].Suit);
                Console.WriteLine(player3[2].Rank + "," + player3[2].Suit);*/


                // print the cards for player 4
              /*  Console.WriteLine(player4[0].Rank + "," + player4[0].Suit);
                Console.WriteLine(player4[1].Rank + "," + player4[1].Suit);
                Console.WriteLine(player4[2].Rank + "," + player4[2].Suit);*/


                // Don't add or modify any code below
                // this comment
                input = Console.ReadLine();
            }
        }
    }
}
