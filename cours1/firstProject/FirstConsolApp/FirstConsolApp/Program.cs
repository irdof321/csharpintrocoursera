﻿using System; //namespace etc

namespace FirstConsolApp //we create a new namespace for the project
{
    /// <summary>
    ///  3 slashes auto generates doc
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
