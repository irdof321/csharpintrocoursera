using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    static int MoveUnitsPerSecond = 5;


    // Update is called once per frame
    void Update()
    {
        float horizontalInput  = Input.GetAxis("Horizontal");
        Vector3 position = transform.position;
        if (horizontalInput != 0)
        {
            position.x += horizontalInput * MoveUnitsPerSecond * Time.deltaTime;
        }
        transform.position = position;
    }
}
