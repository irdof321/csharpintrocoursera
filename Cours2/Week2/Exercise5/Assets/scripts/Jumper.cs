using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
The Jumper class (script) jumps the game object to the mouse location when the left mouse button is pressed.
**/
public class Jumper : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Vector3 position =  Input.mousePosition;
            position.z = -Camera.main.transform.position.z;
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(position);

             // move teddy bear to mouse location
            transform.position = worldPosition;
        }
    }
};
