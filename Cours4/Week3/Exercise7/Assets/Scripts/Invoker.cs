using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


//Invoker a tous les events et permet d'ajouter des listener
public class Invoker : MonoBehaviour
{
    Timer my_timer ;
    MessageEvent messageEvent ;

    void Awake() {
        this.messageEvent = new MessageEvent();
    }

    // Start is called before the first frame update
    void Start()
    {
        this.my_timer  = gameObject.AddComponent<Timer>();
        this.my_timer.Duration = 1;
        this.my_timer.Run();
    }

    // Update is called once per frame
    void Update()
    {
        if(this.my_timer.Finished)
        {
            this.messageEvent.Invoke();
            this.my_timer.Run();
        }
    


    }

    public void AddNoArgumentListener(UnityAction listener)
    {
        this.messageEvent.AddListener(listener);
    }
}
