﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingAssignment4
{
    /// <summary>
    /// Converts words to digits
    /// </summary>
    public class Digitizer
    {
        #region Fields

        // declare your Dictionary field and create the Dictionary object for it here
        private Dictionary<string, int> digit_dic;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Digitizer()
        {
            // populate your dictionary field here
            digit_dic = new Dictionary<string, int>();
            string[] digit_name = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" };
            for (int i = 0; i < 11; i++)
            {
                digit_dic.Add(digit_name[i], i);
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Converts the given word to the corresponding digit.
        /// If the word isn't a valid digit name, returns -1
        /// </summary>
        /// <param name="word">word to convert</param>
        /// <returns>corresponding digit or -1</returns>
        public int ConvertWordToDigit(string word)
        {
            word = word.ToLower();
            try
            {
                return digit_dic[word];
            }catch (KeyNotFoundException)
            {
                return -1;
            }
        }

        #endregion
    }
}
